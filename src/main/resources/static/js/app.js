$(document).ready(function() {
    var token = window.localStorage.getItem('token');
    if (!token){
        window.location.href = "/login.html";
    }
    $.ajax({
        type: 'GET',
        url: '/auth',
        dataType: 'json',
        headers: {
            'X-Auth-Token': token
        },
        success: function(user){
            $('#firstName').text(user.firstName);
            $('#lastName').text(user.lastName);
            $('#birthday').text(new Date(user.birthDay).toDateString());
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            window.localStorage.removeItem('token');
            window.location.href = "/login.html";
        }
    });

    $("#logout").on("click", function(e) {
        window.localStorage.removeItem('token');
        window.location.href = "/index.html";
    })
});