$(document).ready(function() {

    $("#loginForm").on("submit", function(e) {
        e.preventDefault();
        var authObj = {
            username: $('#username').val(),
            password: $('#password').val()
        };
        $.ajax({
            type: 'POST',
            url: '/auth',
            data: JSON.stringify(authObj),
            dataType: "json",
            contentType: "application/json",
            success: function(jwt){
                window.localStorage.setItem('token', jwt.token);
                window.location.href = "/app.html"
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var response = XMLHttpRequest.responseJSON;
                var errorMessage = $('#errorMessage');
                errorMessage.text(response.message);
                errorMessage.removeClass('hidden');
            }
        });
    })
});