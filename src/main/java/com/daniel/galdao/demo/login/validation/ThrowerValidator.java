package com.daniel.galdao.demo.login.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by IFC.Daniel on 03/08/2017.
 */
@Component
public class ThrowerValidator implements Validator {

    private javax.validation.Validator validator;

    public ThrowerValidator() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Object[] violations = validator.validate(target).toArray();
        if (violations.length == 0) {
            return;
        }
        String errorMessage = Arrays.stream(violations)
                .map(v -> (ConstraintViolation<?>) v)
                .map(cv -> cv.getPropertyPath() + " " + cv.getMessage())
                .collect(Collectors.joining("; "));
        throw new IllegalArgumentException(errorMessage);
    }
}
