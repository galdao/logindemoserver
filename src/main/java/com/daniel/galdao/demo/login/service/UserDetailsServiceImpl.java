package com.daniel.galdao.demo.login.service;

import com.daniel.galdao.demo.login.model.User;
import com.daniel.galdao.demo.login.model.security.factory.ApplicationUserFactory;
import com.daniel.galdao.demo.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by IFC.Daniel on 03/08/2017.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user =  userRepository.findOne(username);
        if (user != null){
            return ApplicationUserFactory.create(user);
        }
        throw new UsernameNotFoundException(String.format("Invalid username: '%s'", username));
    }
}
