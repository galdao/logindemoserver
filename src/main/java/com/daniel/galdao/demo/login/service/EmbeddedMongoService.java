package com.daniel.galdao.demo.login.service;

import com.daniel.galdao.demo.login.model.User;
import com.daniel.galdao.demo.login.repository.UserRepository;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version.Main;
import de.flapdoodle.embed.process.runtime.Network;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.Date;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@Service
public class EmbeddedMongoService {

    @Value("${mongodb.embedded.hostname}")
    private String host;

    @Value("${mongodb.embedded.port}")
    private int port;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    private MongodExecutable mongodExecutable;

    /*
     * Um serviço de mongoDB aninhado.
     * Normalmente ele só é utilizado para testes.
     */
    @PostConstruct
    protected void startEmbeddedMongoDb() throws Exception {
        MongodStarter mongodStarter = MongodStarter.getDefaultInstance();
        IMongodConfig iMongoConfig = new MongodConfigBuilder()
                .version(Main.PRODUCTION)
                .net(new Net(host, port, Network.localhostIsIPv6()))
                .build();
        mongodExecutable = mongodStarter.prepare(iMongoConfig);
        mongodExecutable.start();
        addAdminUserToMongo();
    }

    private void addAdminUserToMongo(){
        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword(securityService.hashAndSaltPassword("admin"));
        admin.setFirstName("AdmFirstName");
        admin.setLastName("AdmLastName");
        admin.setBirthDay(new Date());
        admin.setRoles(Collections.singletonList("admin"));
        userRepository.insert(admin);
    }

    @PreDestroy
    protected void stopEmbeddedMongoDb() {
        if (mongodExecutable != null)
            mongodExecutable.stop();
    }

}
