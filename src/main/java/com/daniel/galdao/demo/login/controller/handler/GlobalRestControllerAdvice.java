package com.daniel.galdao.demo.login.controller.handler;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@Configuration
@RestControllerAdvice
public class GlobalRestControllerAdvice {

    @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class})
    protected void handleBadRequestException(Exception ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    protected void handleUsernameNotFoundException(Exception ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
    }

    @ExceptionHandler(HttpMediaTypeException.class)
    protected void handleUnsupportedMediaTypeException(Exception ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(Exception.class)
    protected void handleInternalServerException(Exception ex, HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
    }

}
