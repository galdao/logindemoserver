package com.daniel.galdao.demo.login.utils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by IFC.Daniel on 03/08/2017.
 */
public class KeyUtils {

    public static PrivateKey readPrivateKey(File privateKey) throws Exception {
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(getByteArrayFromFile(privateKey));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    public static PublicKey readPublicKey(File publicKey) throws Exception {
        X509EncodedKeySpec spec = new X509EncodedKeySpec(getByteArrayFromFile(publicKey));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    private static byte[] getByteArrayFromFile(File file) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        byte[] bytes;
        try (DataInputStream dis = new DataInputStream(fis)) {
            bytes = new byte[(int) file.length()];
            dis.readFully(bytes);
        }
        return bytes;
    }

}
