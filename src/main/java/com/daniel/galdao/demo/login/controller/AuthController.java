package com.daniel.galdao.demo.login.controller;

import com.daniel.galdao.demo.login.model.security.AuthenticationRequest;
import com.daniel.galdao.demo.login.model.security.JWTResponse;
import com.daniel.galdao.demo.login.model.User;
import com.daniel.galdao.demo.login.repository.UserRepository;
import com.daniel.galdao.demo.login.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@RestController
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityService securityService;

    @GetMapping
    public User getCurrent(@RequestHeader("X-Auth-Token") String jwt) {
        return securityService.validateAndRetrieveUserFromJWT(jwt);
    }

    @PostMapping
    public JWTResponse authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
        User user = userRepository.findOne(authenticationRequest.getUsername());
        if (user != null) {
            if (securityService.checkPassword(authenticationRequest.getPassword(), user.getPassword())) {
                return new JWTResponse(securityService.prepareJWT(user));
            }
        }
        throw new UsernameNotFoundException("Invalid username and password combination!");
    }

}
