package com.daniel.galdao.demo.login.model.security;

/**
 * Created by IFC.Daniel on 01/08/2017.
 */
public class JWTResponse {

    private String token;

    public JWTResponse() {
    }

    public JWTResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
