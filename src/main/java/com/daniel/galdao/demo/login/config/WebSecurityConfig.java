package com.daniel.galdao.demo.login.config;

import com.daniel.galdao.demo.login.security.CustomCorsFilter;
import com.daniel.galdao.demo.login.security.EntryPointUnauthorizedHandler;
import com.daniel.galdao.demo.login.security.JWTAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private EntryPointUnauthorizedHandler entryPointUnauthorizedHandler;

    @Autowired
    private CustomCorsFilter customCorsFilter;

    @Bean
    public JWTAuthFilter jwtAuthFilter() throws Exception {
        JWTAuthFilter jwtAuthFilter = new JWTAuthFilter();
        jwtAuthFilter.setAuthenticationManager(super.authenticationManagerBean());
        return jwtAuthFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                    .disable()
                .exceptionHandling()
                    .authenticationEntryPoint(entryPointUnauthorizedHandler)
                    .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/auth/**").permitAll()
                    .antMatchers("/user/**").fullyAuthenticated()
                    .and()
                .addFilterBefore(jwtAuthFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(customCorsFilter, CorsFilter.class);
    }

}
