package com.daniel.galdao.demo.login.model.security.factory;

import com.daniel.galdao.demo.login.model.security.ApplicationUser;
import com.daniel.galdao.demo.login.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Collection;

/**
 * Created by IFC.Daniel on 03/08/2017.
 */
public class ApplicationUserFactory {

    public static ApplicationUser create(User user){
        Collection<? extends GrantedAuthority> authorities = getAuthorities(user);
        return new ApplicationUser(
                user.getUsername(),
                user.getPassword(),
                authorities
        );
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(User user){
        return AuthorityUtils.createAuthorityList(user.getRoles().toArray(new String[user.getRoles().size()]));
    }

}
