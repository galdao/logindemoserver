package com.daniel.galdao.demo.login.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
public class User {

    @Id
    @NotNull
    @Length(min = 1, max = 20)
    private String username;

    @NotNull
    @Length(min = 1)
    private String password;

    @NotNull
    @Length(min = 1, max = 20)
    private String firstName;

    @NotNull
    @Length(min = 1, max = 20)
    private String lastName;

    @NotNull
    private Date birthDay;

    @NotNull
    @Size(min = 1)
    private List<String> roles;

    public User() {
    }

    @JsonProperty
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty(required = true)
    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

}
