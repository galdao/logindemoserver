package com.daniel.galdao.demo.login.service;

import com.daniel.galdao.demo.login.model.User;
import com.daniel.galdao.demo.login.utils.KeyUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@Service
public class SecurityService {

    @Value("${security.issuerId}")
    private String issuerId;

    @Value("classpath:public_key.der")
    private Resource publicKeyResource;

    @Value("classpath:private_key.der")
    private Resource privateKeyResource;

    @Autowired
    private ObjectMapper objectMapper;

    private PublicKey publicKey;
    private PrivateKey privateKey;

    @PostConstruct
    private void prepareJWTKeyPair() throws Exception {
        publicKey = KeyUtils.readPublicKey(publicKeyResource.getFile());
        privateKey = KeyUtils.readPrivateKey(privateKeyResource.getFile());
    }

    public String hashAndSaltPassword(String password){
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public boolean checkPassword(String password, String hash){
        return BCrypt.checkpw(password, hash);
    }

    public String prepareJWT(User user){
        return Jwts.builder()
                .claim("user", user)
                .setIssuer(issuerId)
                .setExpiration(Date.from(Instant.now().plus(15, ChronoUnit.MINUTES)))
                .signWith(SignatureAlgorithm.RS256, privateKey)
                .compact();
    }

    public User validateAndRetrieveUserFromJWT(String jwt){
        Claims claims = Jwts.parser()
                .requireIssuer(issuerId)
                .setSigningKey(publicKey)
                .parseClaimsJws(jwt)
                .getBody();
        return objectMapper.convertValue(claims.get("user", Map.class), User.class);
    }

}
