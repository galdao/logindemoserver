package com.daniel.galdao.demo.login.repository;

import com.daniel.galdao.demo.login.model.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends MongoRepository<User, String> {

    @Override
    @Cacheable("user")
    User findOne(String s);

    @Override
    @CacheEvict("user")
    <S extends User> S save(S entity);

}
