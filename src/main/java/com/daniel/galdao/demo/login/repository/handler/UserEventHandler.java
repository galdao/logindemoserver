package com.daniel.galdao.demo.login.repository.handler;

import com.daniel.galdao.demo.login.model.User;
import com.daniel.galdao.demo.login.repository.UserRepository;
import com.daniel.galdao.demo.login.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * Created by IFC.Daniel on 31/07/2017.
 */
@Component
@RepositoryEventHandler(UserRepository.class)
public class UserEventHandler {

    @Autowired
    private SecurityService securityService;

    @HandleBeforeSave
    @HandleBeforeCreate
    public void hashAndSaltPassword(User user){
        user.setPassword(securityService.hashAndSaltPassword(user.getPassword()));
    }

}
