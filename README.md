# Login Demo #

Esse repositório contêm um demo de uma API para o login com JWT usando Spring Boot, Spring Framework, Spring Data, Spring Security, Hibernate Validator e um MongoDB aninhado.

### Como usar ###

* Clonar o repositório
* No diretório do repositório rodar o comando: *mvn spring-boot:run*

### Endereço Página Principal ###

http://localhost:10100

### Endpoints Principais ###

**Autenticação**

* **POST**  http://localhost:10100/auth - Faz o login do usuário, retornando uma JWT.

Corpo da request: {"username":"admin","password":"admin"}


**Gerenciamento de usuários**

* **GET**  http://localhost:10100/user - Retorna todos os usuários registradas
* **GET**  http://localhost:10100/user/{username} - Retorna usuário com username desejado
* **POST**  http://localhost:10100/user - Registra um novo usuário
* **PUT**  http://localhost:10100/user/{username} - Atualiza um usuário existente
* **DELETE**  http://localhost:10100/user/{username} - Deleta um usuário

Header de Autenticação: X-Auth-Token=**JWT**

Corpo da request para requests POST, PUT: {"username":"xxx", "password":"xxx", "firstName":"xxx", "lastName":"xxx", "birthDay":"YYYY-MM-DD", "roles":["xxx","xxx"]}


### Requerimentos ###

* **Java 8**
* **Maven 3.2.5+**